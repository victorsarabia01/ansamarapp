
Qué hace el proyecto

    App movil en contruccion basado en el Framework Expo de React Native que complementara el desarrollo web del proyecto Ansamar Centro Medico, que consiste en el agendamiento de citas y control de pacientes en su evolucion de la salud dental.

Por qué el proyecto es útil

    Luego de hacer el levantamiento de informacion de la comunidad "Ansamar Centro Medico" se determino que sera beneficioso el desarrollo web que contribuya con el agendamiento de citas y control de pacientes en su evolucion de la salud bucal con el tiempo de atencion.

Cómo pueden comenzar los usuarios con el proyecto

    Es necesario que tengan instalado:
    1.- NodeJs: entorno de tiempo de ejecución en tiempo real incluye todo lo que se necesita para ejecutar un programa escrito en JavaScript.
    2.- Instalacion de las librerias a traves del Gestor de Paquetes NPM(Node Package Manager) o en su defecto NPX (Facilita la ejecución de herramientas de línea de comandos que se distribuyen como paquetes, sin tener que instalarlos globalmente.)
    3.- Git para el control de versiones
    4.- Un IDE o entorno de desarrollo integrado que permita usar todas las herramientas necesarias para realizar un proyecto de desarrollo de software. Ejemplo Visual Studio Code
    5.- Aplicacion Expo GO (Play Store) que ayudara a ver en tiempo Real los cambios realizados en el desarrollo del App

Dónde pueden recibir ayuda los usuarios con tu proyecto

    Comunicacion con el programador del App:
    Victor Gabriel Sarabia Pérez
    WhatsApp +58-4245208619
    Email victorgsp94@gmail.com

Quién mantiene y contribuye con el proyecto

    Victor Gabriel Sarabia Perez

import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Dimensions } from 'react-native';
import Svg, { Circle, Rect, Path, Defs, LinearGradient, Stop } from 'react-native-svg';
import ButtonIngresar from './Button';
import { ReactDOM } from 'react';
import { createSlice, configureStore } from '@reduxjs/toolkit'
/*import Svg, {Path, Defs, LinearGradient, Stop} from 'react-native-svg';*/
/*import { LinearGradient } from 'expo-linear-gradient';*/

/* VIEWS --> DIV*/
/* TEXT --> H1*/
export default function App() {

  function SvgTop(){
    return {

    }
  }
  return (
    <View style={styles.container}>
      
      <Text style={styles.title}> Ansamar Centro Médico </Text>
      <Text style={styles.titulo1}>Ingrese su cuenta</Text>
      <StatusBar style="auto" />
      <TextInput 
      placeholder='Usuario'
      secureTextEntry={true}
      style={styles.TextInput}
      />
      <TextInput 
      placeholder='Contraseña'
      style={styles.TextInput}
      secureTextEntry={true}
      />

      <Text style={styles.olvidoPassword}>Recuperar acceso</Text>
      <ButtonIngresar/>
      
    </View>
    
    
    
  );
}

/* ESTILOS PARA NUSTROS COMPONENTES*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titulo: {
    fontSize: 30,
  },
  titulo1: {
    fontSize: 20,
  },
  title: {
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: '#20232a',
    borderRadius: 6,
    backgroundColor: '#61dafb',
    color: '#20232a',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  TextInput: {
   backgroundColor: '#fff',
    padding: 10,
    width: '80%',
    height: 50,
    marginTop: 20,
    borderRadius: 30,
    paddingStart: 30,
  },
  olvidoPassword: {
    marginTop: 10,
  },
});


import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default function ButtonIngresar() {
    return (
        
        <TouchableOpacity style={styles.containerx}>
                <LinearGradient
                // Background Linear Gradient
                colors={['rgba(0,0,0,0.8)', 'transparent']}
                style={styles.background}
                />
                <LinearGradient
                // Button Linear Gradient
                colors={['#008f39', '#008f39', '#008f39']}
                style={styles.button}
                >
                <Text style={styles.text}>Ingresar</Text>
                </LinearGradient>

        </TouchableOpacity>

);
}
/* ESTILOS PARA NUSTROS COMPONENTES*/
const styles = StyleSheet.create({
    containerx: {
        
    },

    text: {
        fontSize: 20,
        color: '#fff',
        
    },
    button: {
        marginTop: 10,
        width: '50%',
        height: 50,
        width: 150,
        borderRadius: 25,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        
    },
    
  });